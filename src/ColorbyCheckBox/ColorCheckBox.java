package ColorbyCheckBox;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;




public class ColorCheckBox {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;

	private JPanel backPanel;
	private JPanel colorPanel;
	private JCheckBox redCheckBox;
	private JCheckBox greenCheckBox;
	private JCheckBox blueCheckBox;
	
	
	public ColorCheckBox(){
		createFrame();
	}
	
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setTitle("ColorCheckBox");
		
		backPanel = new JPanel();
		backPanel.setLayout(new BorderLayout());
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		backPanel.add(colorPanel, BorderLayout.SOUTH);
		
		
		frame.add(backPanel,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setVisible(true);
		
		createCheckBox();
	
		
	}
	
	

	class ColorListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(redCheckBox.isSelected()&&greenCheckBox.isSelected()&&blueCheckBox.isSelected()){
					backPanel.setBackground(new Color(0,0,0));
			}
			
			else if(redCheckBox.isSelected()&&greenCheckBox.isSelected()){
					backPanel.setBackground(new Color(255, 215 ,0));
			}
			else if(redCheckBox.isSelected()&&blueCheckBox.isSelected()){
					backPanel.setBackground(new Color(139, 34 ,82));
			}
			
			else if(greenCheckBox.isSelected()&&redCheckBox.isSelected()){
					backPanel.setBackground(new Color(255, 215 ,0));
			}
				
			else if(greenCheckBox.isSelected()&&blueCheckBox.isSelected()){
					backPanel.setBackground(new Color(0, 134,139));
			}
			else if(blueCheckBox.isSelected()&&redCheckBox.isSelected()){
					backPanel.setBackground(new Color(139, 34 ,82));
					
			}
				
			else if (blueCheckBox.isSelected()&&greenCheckBox.isSelected()){
					backPanel.setBackground(new Color(0, 134,139));
					
			}
			else if (redCheckBox.isSelected()){
				backPanel.setBackground(new Color(178, 34, 34));
			}
			else if (greenCheckBox.isSelected()){
				backPanel.setBackground(new Color(0, 205, 0));
			}
			else if (blueCheckBox.isSelected()){
				backPanel.setBackground(new Color(0 ,0 ,238));
			}
	
		}
		
	}
	
	
	public JPanel createCheckBox(){
		
		
		ActionListener listener = new ColorListener();
		
		redCheckBox = new JCheckBox("Red");
		redCheckBox.addActionListener(listener);
		
		greenCheckBox = new JCheckBox("Green");
		greenCheckBox.addActionListener(listener);
		
		blueCheckBox = new JCheckBox("Blue");
		blueCheckBox.addActionListener(listener);
		
		
		colorPanel.add(redCheckBox);
		colorPanel.add(greenCheckBox);
		colorPanel.add(blueCheckBox);
		
		return colorPanel;
	
	}
}
	