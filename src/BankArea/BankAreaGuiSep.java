package BankArea;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BankAreaGuiSep {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 200;
	
	private JPanel panelTop;
	private JTextField balanceTextfield;
	private JButton depositButton;
	private JButton withdrawButton;
	private JLabel balanceLabel;
	private JTextArea resultArea;
	private int amount;
	private String showresult;
	private String showorder;

	
	public BankAreaGuiSep(){
	
		createFrame();
	}
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setTitle("BankAccount Area");

		createPanel();
		frame.add(panelTop,BorderLayout.CENTER);
		
	
		
	}


	
	

	public void createPanel() {
		
		balanceLabel = new JLabel("Enter your  balance : ");
		balanceTextfield = new JTextField(10);

		depositButton = new JButton("Deposit");
		withdrawButton = new JButton("Withdraw");
		
		resultArea = new JTextArea(5, 20);
		
		
		panelTop = new JPanel();
		panelTop.add(balanceLabel);
		panelTop.add(balanceTextfield);
		panelTop.add(depositButton);
		panelTop.add(withdrawButton);
		
		
		panelTop.add(resultArea);
	
	
	}	
	
	public int getText(){
		amount = Integer.parseInt(balanceTextfield.getText());
		return amount;
	}
	
	public void setTextresultDeposit(String str){
		showresult = "Balance in account : "+str;
		resultArea.append("Your deposit amount : "+str+"\n");
		resultArea.append(showresult+"\n");
	}
	public void setTextresultWithdraw(String str){
		showresult = "Balance in account : "+str;
		resultArea.append("Your withdraw amount : "+str+"\n");
		resultArea.append(showresult+"\n");
	}
	
	public void setDepositListener(ActionListener listener){
		
		depositButton.addActionListener(listener);
	}
	
	public void setWithdrawListener(ActionListener listener){
		
		withdrawButton.addActionListener(listener);
	}
	

}
