package ColorbyButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class ColorButtonFrame {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;

	private JPanel backPanel;
	private JPanel colorPanel;
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
	
	
	public ColorButtonFrame(){
		createFrame();
	}
	
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setTitle("ColorButton");
		
		backPanel = new JPanel();
		backPanel.setLayout(new BorderLayout());
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		backPanel.add(colorPanel, BorderLayout.SOUTH);
		
		
		frame.add(backPanel,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setVisible(true);
		
		createButton();
	
		
	}
	
	class RedListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			backPanel.setBackground(new Color(205, 51 ,51));
			
			
		}
		
	}
	
	class GreenListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			backPanel.setBackground(new Color(154, 205 ,50));
			
		}
		
	}
	
	class BlueListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			backPanel.setBackground(new Color(0, 0 ,205));
			
		}
		
	}
	
	
	
	public void createButton(){
		
		ActionListener redlistener =  new RedListener();
		ActionListener greenlistener = new GreenListener();
		ActionListener bluelistener = new BlueListener();
		
		redButton = new JButton("Red");
		redButton.addActionListener(redlistener);
		
		greenButton = new JButton("Green");
		greenButton.addActionListener(greenlistener);
		
		blueButton = new JButton("Blue");
		blueButton.addActionListener(bluelistener);
		
		colorPanel.add(redButton);
		colorPanel.add(greenButton);
		colorPanel.add(blueButton);
		
		
	}
	
	
	

}
