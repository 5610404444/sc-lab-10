package ColorbyMenu;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;



public class ColorMenu {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;

	private JPanel backPanel;
	private String nameColor;
	
	
	public ColorMenu(){
		createFrame();
	}
	
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setTitle("ColorMenu");
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		menuBar.add(createColorMenu());
		
		nameColor = "Red";
		
		
		backPanel = new JPanel();
		backPanel.setLayout(new BorderLayout());
		
		
		frame.add(backPanel,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setVisible(true);
		

	
		
	}
	
	public JMenu createColorMenu() {
		JMenu menu = new JMenu("Color");
		menu.add(createColorItem("Red"));
		menu.add(createColorItem("Green"));
		menu.add(createColorItem("Blue"));
		
		return menu;
	}


	public JMenuItem createColorItem(final String name){
		
		class ColorListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				
				nameColor = name;
				setColor(nameColor);
			
				
				
			}

		}
		JMenuItem item = new JMenuItem(name);
		ActionListener listener = new ColorListener();
		item.addActionListener(listener);
		return item;
	}
	
	private void setColor(String nameColor) {
		if(nameColor=="Red"){
			backPanel.setBackground(new Color(205, 51 ,51));
			
		}
		else if(nameColor=="Green"){
			backPanel.setBackground(new Color(154, 205 ,50));
			
		}
		else if(nameColor=="Blue"){
			backPanel.setBackground(new Color(0, 0 ,205));
			
		}
		
	}
	
	
}
	
