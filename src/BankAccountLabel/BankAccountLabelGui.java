package BankAccountLabel;

import java.awt.BorderLayout;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BankAccountLabelGui {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 200;
	
	private JPanel panelTop;
	private JTextField balanceTextfield;
	private JButton depositButton;
	private JButton withdrawButton;
	private JLabel balanceLabel;
	private JLabel resultLabel;
	private String showresult;
	private int amount;

	
	
	
	public BankAccountLabelGui(){
		
		createFrame();
	}
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setTitle("BankAccount Area");

		createPanel();
		frame.add(panelTop,BorderLayout.CENTER);
		
	
		
	}


	
	

	public void createPanel() {
		
		balanceLabel = new JLabel("Enter your  balance : ");
		balanceTextfield = new JTextField(10);

		depositButton = new JButton("Deposit");
		withdrawButton = new JButton("Withdraw");
		
		
		resultLabel = new JLabel();
		
		
		panelTop = new JPanel();
		panelTop.add(balanceLabel);
		panelTop.add(balanceTextfield);
		panelTop.add(depositButton);
		panelTop.add(withdrawButton);
		
		
		panelTop.add(resultLabel);
	
	
}


	public int getText(){
		amount = Integer.parseInt(balanceTextfield.getText());
		return amount;
	}
	
	public void setTextresult(String str){
		showresult = "Balance in account : "+str;
		
		resultLabel.setText(showresult);
	}
	
	public void setDepositListener(ActionListener listener){
		depositButton.addActionListener(listener);
	}
	
	public void setWithdrawListener(ActionListener listener){
		withdrawButton.addActionListener(listener);
	}
	

}
