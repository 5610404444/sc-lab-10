package BankAccountLabel;

public class BankAccount {
	
	private double balance;
	
	public BankAccount(){
		balance = 0;
	}
	
	public BankAccount(double initialBalance){
		balance = initialBalance;
	}
	
	public void Deposit(double amount){
		balance = balance + amount;
	}
	
	public void Withdraw(double amount){
		balance = balance-amount;
	}
	
	public void monthlyFee(){
		this.Withdraw(10);
		
	}
	
	public void monthEnd(){
		
	}
	
	public double getBalalnce(){
		return balance;
	}
}
