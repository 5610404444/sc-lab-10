package ColorbyComboBox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;




public class ColorComboBox {
	
	private static final int FRAME_WIDTH = 300; 
	private static final int FRAME_HEIGHT = 400;

	private JPanel backPanel;
	private JPanel colorPanel;
	private JComboBox colorNameCombo;
	
	
	
	public ColorComboBox(){
		createFrame();
	}
	
	
	public void createFrame(){
		
		JFrame frame = new JFrame();
		frame.setTitle("ColorComboBox");
		
		backPanel = new JPanel();
		backPanel.setLayout(new BorderLayout());
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		backPanel.add(colorPanel, BorderLayout.SOUTH);
		
		
		frame.add(backPanel,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setVisible(true);
		
		createComboBox();
	
		
	}
	
	class ComboListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			String colorName = (String) colorNameCombo.getSelectedItem();
			
			if(colorName=="Red"){
				backPanel.setBackground(new Color(205, 51 ,51));
				
			}
			else if(colorName=="Green"){
				backPanel.setBackground(new Color(154, 205 ,50));
				
			}
			else if(colorName=="Blue"){
				backPanel.setBackground(new Color(0, 0 ,205));
				
			}
			
			
		}
		
	}
	

	
	
	public JPanel createComboBox(){
		
		
		colorNameCombo = new JComboBox();
		colorNameCombo.addItem("Red");
		colorNameCombo.addItem("Green");
		colorNameCombo.addItem("Blue");
		colorNameCombo.setEditable(false);
		ActionListener combolistener =  new ComboListener();
		colorNameCombo.addActionListener(combolistener);
		
		
		
		colorPanel.add(colorNameCombo);
		return colorPanel;
	}
	
	
	

}
